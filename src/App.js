import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import View from '@vkontakte/vkui/dist/components/View/View';
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';
import '@vkontakte/vkui/dist/vkui.css';
import {ModalCard, ModalRoot} from '@vkontakte/vkui';
import { YMaps, Map, Placemark } from 'react-yandex-maps';
import Icon24Place from '@vkontakte/icons/dist/24/place';
import './App.css';

import Home from './panels/Home';
import Coffee from './panels/Coffee'; 

const App = () => {
	const [activePanel, setActivePanel] = useState('home');
	const [fetchedUser, setUser] = useState(null);
	const [popout, setPopout] = useState(<ScreenSpinner size='large' />);
	const [nearestPartner, setNearestPartner] = useState({
		coords: [55.75, 37.57],
		name: 'Default Location'
	});
	const [activeModal, setActiveModal] = useState(null);
	const [partnersList, setPartnersList] = useState([
		{
			name: 'Saint-Petersburg Starbucks',
			coords: [59.932140, 30.350611]
		},
		{
			name: 'Moscow Starbucks',
			coords: [55.757311, 37.616656]
		},
		{
			name: 'Sevastopol Starbucks',
			coords: [44.589579, 33.458337]
		}
	]);
	const [winCoord, setWinCoord] = useState({x: Math.floor(Math.random() * 10) + 1, y: Math.floor(Math.random() * 10) + 1});
	const PARTNER_FOUND = 'partner_found';
	const MODAL_CARD_TO_THE_WALL = 'push_to_wall'

	let findPartner = () => {
		bridge.sendPromise('VKWebAppGetGeodata', {})
		.then((data) => {
			let partners = partnersList;
			let shortestDistance = Math.sqrt(Math.abs((data.lat - partners[0].coords[0])^2 + (data.long - partners[0].coords[1])^2));
			setNearestPartner(partners[0]);
			partners.forEach((partner, index) => {
				let currentDistance = Math.sqrt(Math.abs((data.lat - partner.coords[0])^2 + (data.long - partner.coords[1])^2));
				if (currentDistance < shortestDistance) {
					setNearestPartner(partner);
				}
			});
			setActiveModal(PARTNER_FOUND);
		});
	}
	
	let selectPartner = () => {
		setActivePanel('partnersList');
	}

	useEffect(() => {
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
				document.body.attributes.setNamedItem(schemeAttribute);
			}
		});
		async function fetchData() {
			const user = await bridge.send('VKWebAppGetUserInfo');
			setUser(user);
			setPopout(null);
		}
		fetchData();
	}, []);

	const go = e => {
		setActivePanel(e.currentTarget.dataset.to);
	};

	const pushToTheWall = () => {
		bridge.send("VKWebAppShowWallPostBox", {"message": "Я выиграл чашку кофе в культовой игре Горячо-Холодно, попробуй и ты. https://vk.com/app7341785_799258"});;
	}

	const victoryMessage = () => {
		setActiveModal(MODAL_CARD_TO_THE_WALL);
	}

	const modal = (
		<ModalRoot activeModal={activeModal}>
			<ModalCard
				id={'partner_found'}
				header="Ближайшее предложение"
				caption={nearestPartner.name}
				actions={[{
					title: 'Выиграть приз',
					mode: 'primary',
					action: () => {
						setActiveModal(null);
						setActivePanel('coffee');
					}
				}]} >
				<YMaps>
					<div className='flex-center'>
						<Map defaultState={{ center: [...nearestPartner.coords], zoom: 9 }} >
							<Placemark geometry={[...nearestPartner.coords]} />
						</ Map>
					</div>
				</YMaps>
        	</ModalCard>
			<ModalCard
				id={MODAL_CARD_TO_THE_WALL}
				onClose={() => setActiveModal(null)}
				header="Хотите рассказать о победе на своей стене?"
				actions={[{
					title: 'Разместить на стене',
					mode: 'primary',
					action: () => {
						pushToTheWall();
						setActiveModal(null);
						setActivePanel('home');
					}
				}]}
				>

			</ModalCard>
		</ModalRoot>
	)

	return (
		<View activePanel={activePanel} popout={popout} modal={modal}>
			<Home id='home' fetchedUser={fetchedUser} go={go} findCafe={findPartner} findPartner={findPartner} selectPartner={selectPartner} />
			<Coffee 
				id='coffee' 
				victoryMessage={victoryMessage}
				partner={nearestPartner}
				winCoord={winCoord}
				go={go} />
		</View>
	);
}

export default App;

