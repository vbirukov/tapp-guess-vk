import React from 'react'
import classes from './myButton.css'

const myButton = props => {
  const cls = [classes.AnswerItem]

  if (props.state) {
    cls.push(classes[props.state])
  }

  return (
    <button>
      { props.clicked ? 'X' : '?' }
    </button>
  )
}

export default myButton